#!/bin/bash

export DISPLAY=:0
unclutter &

source /home/pi/startups/npmStartup.sh & source /home/pi/startups/nodeStartup.sh &

#sed -i 's/"exited_cleanly":false/"exited_cleanly":true/'
#/home/pi/.config/chromium/Default/Preferences
#sed -i 's/"exit_type":"Crashed"/"exit_type":"Normal"/'
#/home/pi/.config/chromium/Default/Preferences

/usr/bin/chromium-browser --window-size=480,320 --kiosk --window-position=0,0 https://localhost:3000 &

